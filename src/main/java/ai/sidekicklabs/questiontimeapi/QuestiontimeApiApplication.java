package ai.sidekicklabs.questiontimeapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuestiontimeApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(QuestiontimeApiApplication.class, args);
	}

}
