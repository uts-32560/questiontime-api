package ai.sidekicklabs.questiontimeapi.config;

import ai.sidekicklabs.questiontimeapi.model.Account;
import ai.sidekicklabs.questiontimeapi.repo.AccountRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private AccountRepo accounts ;


    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/accounts").antMatchers("/error") ;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors().and().csrf().disable()
                .authorizeRequests().anyRequest().authenticated().and().httpBasic() ;
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {

        auth.userDetailsService(username -> {

            Account account = accounts.getByUsername(username) ;
            return User.withUsername(username)
                    .password("{noop}" + account.getPassword())
                    .roles(account.getRoles())
                    .build() ;
        }) ;
    }

}
