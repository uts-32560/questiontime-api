package ai.sidekicklabs.questiontimeapi.controller;

import ai.sidekicklabs.questiontimeapi.model.Answer;
import ai.sidekicklabs.questiontimeapi.model.Question;
import ai.sidekicklabs.questiontimeapi.model.Room;
import ai.sidekicklabs.questiontimeapi.repo.AnswerRepo;
import ai.sidekicklabs.questiontimeapi.repo.QuestionRepo;
import ai.sidekicklabs.questiontimeapi.repo.RoomRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@Controller
public class AnswerController {

    @Autowired
    private RoomRepo rooms ;

    @Autowired
    private QuestionRepo questions ;

    @Autowired
    private AnswerRepo answers ;

    @GetMapping("/rooms/{roomId}/questions/current/answers")
    public @ResponseBody
    List<Answer> getAnswers(
            @PathVariable String roomId
    ) throws RoomController.RoomNotFound, QuestionController.QuestionNotFound {

        Room room = rooms.getById(roomId) ;

        if (room == null)
            throw new RoomController.RoomNotFound() ;

        Question question = questions.getCurrent(roomId);

        if (question == null)
            throw new QuestionController.QuestionNotFound() ;

        return answers.findForQuestion(question.getId()) ;
    }


    @PostMapping("/rooms/{roomId}/questions/current/answers")
    public @ResponseBody
    Answer createAnswer(
            @PathVariable String roomId,
            @RequestBody SimpleAnswer answer,
            Authentication auth
    ) throws RoomController.RoomNotFound, QuestionController.QuestionNotFound {

        Room room = rooms.getById(roomId) ;

        if (room == null)
            throw new RoomController.RoomNotFound() ;

        Question question = questions.getCurrent(roomId);

        if (question == null)
            throw new QuestionController.QuestionNotFound() ;

        Answer existingAnswer = answers.getByQuestionAndCreator(question.getId(), auth.getName()) ;

        if (existingAnswer != null) {
            existingAnswer.updateText(answer.text);
            answers.save(existingAnswer) ;
            return existingAnswer;
        }

        Answer newAnswer = new Answer(auth.getName(), roomId, question.getId(), answer.text) ;
        answers.save(newAnswer) ;
        return newAnswer ;
    }



    public static class SimpleAnswer {
        public String text ;
    }
}
