package ai.sidekicklabs.questiontimeapi.controller;

import ai.sidekicklabs.questiontimeapi.model.Answer;
import ai.sidekicklabs.questiontimeapi.model.Question;
import ai.sidekicklabs.questiontimeapi.model.Room;
import ai.sidekicklabs.questiontimeapi.repo.AnswerRepo;
import ai.sidekicklabs.questiontimeapi.repo.QuestionRepo;
import ai.sidekicklabs.questiontimeapi.repo.RoomRepo;
import ai.sidekicklabs.questiontimeapi.services.ClustererService;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin
@Controller
public class ClusterController {


    @Autowired
    private RoomRepo rooms ;

    @Autowired
    private QuestionRepo questions ;

    @Autowired
    private AnswerRepo answers ;


    @Autowired
    private ClustererService clusterer ;


    @GetMapping("/rooms/{roomId}/questions/current/answerclusters")
    public @ResponseBody
    List<List<String>> getAnswerClusters(
            @PathVariable String roomId
    ) throws RoomController.RoomNotFound, QuestionController.QuestionNotFound, IOException, UnirestException {

        Room room = rooms.getById(roomId) ;

        if (room == null)
            throw new RoomController.RoomNotFound();

        Question question = questions.getCurrent(roomId) ;

        if (question == null)
            throw new QuestionController.QuestionNotFound() ;

        List<String> answerTexts = new ArrayList<>() ;

        for (Answer answer: answers.findForQuestion(question.getId())) {

            answerTexts.add(answer.getText()) ;
        }

        return  clusterer.getClusters(answerTexts) ;

    }



}
