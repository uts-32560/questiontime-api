package ai.sidekicklabs.questiontimeapi.controller;


import ai.sidekicklabs.questiontimeapi.model.Question;
import ai.sidekicklabs.questiontimeapi.model.Room;
import ai.sidekicklabs.questiontimeapi.repo.QuestionRepo;
import ai.sidekicklabs.questiontimeapi.repo.RoomRepo;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@Controller
public class QuestionController {


    @Autowired
    private RoomRepo rooms ;

    @Autowired
    private QuestionRepo questions ;


    @GetMapping("/rooms/{roomId}/questions/current")
    public @ResponseBody
    Question getCurrentQuestion(
            @PathVariable String roomId
    ) throws RoomController.RoomNotFound, QuestionNotFound {

        Room room = rooms.getById(roomId) ;

        if (room == null)
            throw new RoomController.RoomNotFound() ;

        Question question = questions.getCurrent(roomId);

        if (question == null)
            throw new QuestionNotFound() ;

        return question ;
    }

    @Secured("LECTURER")
    @PostMapping("/rooms/{roomId}/questions")
    public @ResponseBody Question createQuestion(

            @PathVariable String roomId,
            @RequestBody SimpleQuestion question,
            Authentication auth

    ) throws RoomController.RoomNotFound, InvalidQuestion {

        Room room = rooms.getById(roomId) ;

        if (room == null)
            throw new RoomController.RoomNotFound() ;

        if (question.text == null)
            throw new InvalidQuestion("Missing question text") ;

        Question newQuestion = new Question(roomId, question.text, auth.getName()) ;

        questions.save(newQuestion);

        //record that room has been updated, so it will be shown at top of lists
        room.updateLastActiveAt();
        rooms.save(room) ;

        return newQuestion ;
    }


    public static class SimpleQuestion {
        public String text ;
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public static class InvalidQuestion extends Exception {

        public InvalidQuestion(String message) {
            super(message) ;
        }
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public static class QuestionNotFound extends Exception {

        public QuestionNotFound() {
            super("Question does not exist") ;
        }
    }

}
