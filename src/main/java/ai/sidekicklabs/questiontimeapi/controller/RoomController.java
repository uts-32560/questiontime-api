package ai.sidekicklabs.questiontimeapi.controller;

import ai.sidekicklabs.questiontimeapi.model.Question;
import ai.sidekicklabs.questiontimeapi.model.Room;
import ai.sidekicklabs.questiontimeapi.repo.QuestionRepo;
import ai.sidekicklabs.questiontimeapi.repo.RoomRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@CrossOrigin
@Controller
public class RoomController {

    @Autowired
    private RoomRepo rooms ;

    @Autowired
    private QuestionRepo questions ;


    @GetMapping("/rooms/{roomId}")
    public @ResponseBody RoomWithQuestion getRoom(
            @PathVariable String roomId
    ) throws RoomNotFound {

        Room room = rooms.getById(roomId) ;

        if (room == null)
            throw new RoomNotFound() ;

        return new RoomWithQuestion(room, questions.getCurrent(roomId)) ;
    }

    @Secured("LECTURER")
    @PostMapping("/rooms")
    public @ResponseBody Room createRoom(
            @RequestBody SimpleRoom room,
            Authentication auth
    ) throws InvalidRoom {

        if (room.id == null)
            throw new InvalidRoom("Missing room id") ;

        if (rooms.roomIdExists(room.id))
            throw new InvalidRoom("Room id is already in use") ;

        Room newRoom = new Room(
                room.id,
                auth.getName()
        ) ;

        rooms.save(newRoom) ;
        return newRoom ;
    }

    @GetMapping("/rooms")
    public @ResponseBody List<Room> getRooms(
            @RequestParam(required = false) Date lastActiveBefore,
            Authentication auth
    ) {
        return rooms.findRooms(lastActiveBefore, auth.getName()) ;
    }








    public static class SimpleRoom {

        public String id ;
    }


    public static class RoomWithQuestion extends Room {

        private String currentQuestion ;

        public RoomWithQuestion(Room room, Question question) {

            super(room);

            if (question != null)
                this.currentQuestion = question.getText() ;
        }

        public String getCurrentQuestion() {
            return currentQuestion;
        }
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public static class InvalidRoom extends Exception {

        public InvalidRoom(String message) {
            super(message) ;
        }
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public static class RoomNotFound extends Exception {

        public RoomNotFound() {
            super("Room does not exist") ;
        }
    }
}


