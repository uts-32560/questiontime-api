package ai.sidekicklabs.questiontimeapi.model;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document
public class Account {

    @Id
    private String username ;

    private String password ;

    @Indexed
    private String[] roles ;

    @Indexed
    private Date createdAt ;

    private Account() {

    }

    public Account(String username, String password, String[] roles) {
        this.username = username;
        this.password = password;
        this.roles = roles;

        this.createdAt = new Date() ;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String[] getRoles() {
        return roles;
    }

    public Date getCreatedAt() {
        return createdAt;
    }
}
