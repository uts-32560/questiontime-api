package ai.sidekicklabs.questiontimeapi.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document
public class Answer {

    @Id
    private ObjectId id ;

    @Indexed
    private String createdBy ;

    @Indexed
    private String roomId ;

    @Indexed
    private ObjectId questionId ;

    private String text ;

    @Indexed
    private Date createdAt ;


    public Answer() {

    }

    public Answer(String createdBy, String roomId, ObjectId questionId, String text) {
        this.createdBy = createdBy;
        this.roomId = roomId ;
        this.questionId = questionId;
        this.text = text;
        this.createdAt = new Date();
    }

    public ObjectId getId() {
        return id;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public String getRoomId() {
        return roomId ;
    }

    public ObjectId getQuestionId() {
        return questionId;
    }

    public String getText() {
        return text;
    }

    public void updateText(String text) {
        this.text = text ;
        this.createdAt = new Date() ;
    }

    public Date getCreatedAt() {
        return createdAt;
    }
}
