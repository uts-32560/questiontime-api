package ai.sidekicklabs.questiontimeapi.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document
public class Question {

    @Id
    private ObjectId id ;

    @Indexed
    private String roomId ;

    @Indexed
    private String text ;

    @Indexed
    private String createdBy ;

    @Indexed
    private Date createdAt ;


    private Question() {

    }

    public Question(String roomId, String text, String createdBy) {
        this.roomId = roomId;
        this.text = text;
        this.createdBy = createdBy ;
        this.createdAt = new Date() ;
    }

    public ObjectId getId() {
        return id;
    }

    public String getRoomId() {
        return roomId;
    }

    public String getText() {
        return text;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public Date getCreatedAt() {
        return createdAt;
    }
}
