package ai.sidekicklabs.questiontimeapi.model;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document
public class Room {

    @Indexed
    private String id ;

    @Indexed
    private String createdBy ;

    @Indexed
    private Date createdAt ;


    @Indexed
    private Date lastActiveAt ;

    private Room() {

    }

    public Room(String id, String createdBy) {
        this.id = id;
        this.createdBy = createdBy;
        this.createdAt = new Date() ;
        this.lastActiveAt = new Date() ;
    }

    public Room(Room room) {
        this.id = room.id ;
        this.createdBy = room.createdBy ;
        this.createdAt = room.createdAt ;
        this.lastActiveAt = room.lastActiveAt ;
    }

    public String getId() {
        return id;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Date getLastActiveAt() {
        return lastActiveAt;
    }

    public void updateLastActiveAt() {
        this.lastActiveAt = new Date() ;
    }
}
