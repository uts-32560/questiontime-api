package ai.sidekicklabs.questiontimeapi.repo;

import ai.sidekicklabs.questiontimeapi.model.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;


@Repository
public class AccountRepo {

    @Autowired
    private MongoTemplate mongo ;

    public boolean usernameExists(String username) {

        Query query = new Query()
                .addCriteria(Criteria.where("username").is(username)) ;

        return mongo.exists(query, Account.class) ;
    }

    public Account getByUsername(String username) {

        return mongo.findById(username, Account.class)  ;
    }

    public List<Account> findAccounts(
         Date createdBefore,
         String role
    ) {

        Query query = new Query() ;

        if (createdBefore != null) {
            query.addCriteria(Criteria.where("createdAt").lt(createdBefore)) ;

        }

        if (role != null) {
            query.addCriteria(Criteria.where("roles").is(role)) ;
        }

        query
                .limit(10)
                .with(Sort.by(Sort.Direction.DESC, "createdAt")) ;

        return mongo.find(query, Account.class) ;

    }

    public void save(Account account) {

        mongo.save(account) ;
    }
}
