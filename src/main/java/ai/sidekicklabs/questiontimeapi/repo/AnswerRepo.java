package ai.sidekicklabs.questiontimeapi.repo;

import ai.sidekicklabs.questiontimeapi.model.Answer;
import ai.sidekicklabs.questiontimeapi.model.Question;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AnswerRepo {

    @Autowired
    private MongoTemplate mongo ;

    public Answer getById(ObjectId answerId) {

        return mongo.findById(answerId, Answer.class)  ;
    }

    public Answer getByQuestionAndCreator(ObjectId questionId, String createdBy) {

        Query query = new Query()
                .addCriteria(Criteria.where("questionId").is(questionId))
                .addCriteria(Criteria.where("createdBy").is(createdBy));

        return mongo.findOne(query, Answer.class) ;
    }

    public List<Answer> findForQuestion(ObjectId questionId) {

        Query query = new Query()
                .addCriteria(Criteria.where("questionId").is(questionId))
                .with(Sort.by(Sort.Direction.DESC, "createdAt")) ;

        return mongo.find(query, Answer.class) ;
    }

    public void save(Answer answer) {
         mongo.save(answer) ;
    }

}
