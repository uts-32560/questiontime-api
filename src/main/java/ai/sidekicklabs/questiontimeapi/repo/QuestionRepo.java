package ai.sidekicklabs.questiontimeapi.repo;

import ai.sidekicklabs.questiontimeapi.model.Question;
import ai.sidekicklabs.questiontimeapi.model.Room;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public class QuestionRepo {

    @Autowired
    private MongoTemplate mongo ;

    public Question getById(ObjectId questionId) {

        return mongo.findById(questionId, Question.class)  ;
    }

    public Question getCurrent(String roomId) {

        Query query = new Query()
                .addCriteria(Criteria.where("roomId").is(roomId))
                .with(Sort.by(Sort.Direction.DESC, "createdAt")) ;

        return mongo.findOne(query, Question.class) ;
    }

    public List<Question> findQuestions(
            String roomId,
            Date createdBefore
    ) {

        Query query = new Query() ;

        if (roomId != null) {
            query.addCriteria(Criteria.where("roomId").is(roomId)) ;
        }

        if (createdBefore != null) {
            query.addCriteria(Criteria.where("createdAt").lt(createdBefore)) ;
        }

        query
                .limit(10)
                .with(Sort.by(Sort.Direction.DESC, "createdAt")) ;

        return mongo.find(query, Question.class) ;
    }


    public void save(Question question) {

        mongo.save(question) ;
    }



}
