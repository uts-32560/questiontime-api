package ai.sidekicklabs.questiontimeapi.repo;

import ai.sidekicklabs.questiontimeapi.model.Account;
import ai.sidekicklabs.questiontimeapi.model.Room;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public class RoomRepo {

    @Autowired
    private MongoTemplate mongo ;

    public boolean roomIdExists(String roomId) {

        Query query = new Query()
                .addCriteria(Criteria.where("id").is(roomId)) ;

        return mongo.exists(query, Room.class) ;
    }

    public Room getById(String roomId) {

        return mongo.findById(roomId, Room.class)  ;
    }

    public List<Room> findRooms(
            Date lastActiveBefore,
            String createdBy
    ) {

        Query query = new Query() ;

        if (lastActiveBefore != null) {
            query.addCriteria(Criteria.where("lastActiveAt").lt(lastActiveBefore)) ;

        }

        if (createdBy != null) {
            query.addCriteria(Criteria.where("createdBy").is(createdBy)) ;
        }

        query
                .limit(10)
                .with(Sort.by(Sort.Direction.DESC, "lastActiveAt")) ;

        return mongo.find(query, Room.class) ;
    }

    public void save(Room room) {

        mongo.save(room) ;
    }


}
