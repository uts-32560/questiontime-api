package ai.sidekicklabs.questiontimeapi.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class ClustererService {


    @Value("${clusterer.api}")
    private String clustererApi ;

    private ObjectMapper mapper = new ObjectMapper() ;

    public List<List<String>> getClusters(List<String> answers) throws IOException, UnirestException {

        Request req = new Request(answers) ;

        String jsonReq = mapper.writeValueAsString(req) ;

        HttpResponse<String> response = Unirest.post(clustererApi + "/clusters")
                .header("Content-type", "application/json")
                .body(jsonReq)
                .asString() ;

        if (response.getStatus() != 200)
            return null ;

        String respJson = response.getBody() ;

        Response resp = mapper.readValue(respJson, Response.class) ;

        return resp.clusters ;
    }


    public static class Request {

        public List<String> sentences ;

        public Request(List<String> sentences) {
            this.sentences = sentences;
        }
    }

    public static class Response {

        public List<List<String>> clusters ;
    }

}
